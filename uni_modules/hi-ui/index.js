/**
 * hiui
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
const $hi = {
    // 配置
    config: {
        // 组件公共属性
        componentsOptions: {
            // 在组件定义时的选项中启动多 slot 支持
            multipleSlots: true,

            // 将自定义节点设置成虚拟的，更加接近 Vue 组件的表现。
            // 头条小程序关闭该设置，因为开启后
            //  - 自定义组件节点上的 id、class、style 将不再生效； ​
            //  - 自定义组件节点上绑定的事件将不再生效；
            //  - getRelationNodes 获取组件间关系将不再生效； ​
            //  - ​组件实例上 selectComponent、selectAllComponents 异步 API 将不再生效；
            //  - SJS 事件回调函数、SJS 属性监听函数中元素所在自定义组件的ComponentDescriptor 方法中，setStyle、addClass/removeClass/hasClass、getDataset、triggerEvent、getComputedStyle和getBoundingClientRect 将不再生效，selectComponent 和selectAllComponents 将更改为从根节点使用选择器选择组件实例节点。​
            // 反正是各种不生效。参考链接：https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/framework/custom-component/component-model-and-style
            // #ifndef MP-TOUTIAO
            virtualHost: true,
            // #endif

            // [组件样式隔离方式](https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/wxml-wxss.html#%E7%BB%84%E4%BB%B6%E6%A0%B7%E5%BC%8F%E9%9A%94%E7%A6%BB)
            // - isolated 表示启用样式隔离，在自定义组件内外，使用 class 指定的样式将不会相互影响（一般情况下的默认值）；
            // - apply-shared 表示页面 wxss 样式将影响到自定义组件，但自定义组件 wxss 中指定的样式不会影响页面；
            // - shared 表示页面 wxss 样式将影响到自定义组件，自定义组件 wxss 中指定的样式也会影响页面和其他设置了 apply-shared 或 shared 的自定义组件。（这个选项在插件中不可用。）
            // 微信小程序在组件 options 中配置，支付宝小程序在 manifest.json -> mp-alipay 中配置
            styleIsolation: "apply-shared"
        },

        // 图标组件配置
        icon: {
            // 扩展图标的类名前缀，如果想扩展多个图标库，将值设置为数组就可以
            // 例如：["app-iconfont", "my-iconfont"]
            prefix: ""
        }
    }
};
uni.$hi = $hi;
export default $hi;