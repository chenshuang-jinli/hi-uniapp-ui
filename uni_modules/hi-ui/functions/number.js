/**
 * @descript Number 类型数据处理工具函数集
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 *
 * @list 函数列表
 * ========================================================================================================================
 * = floatFormat       : 格式化浮点数
 * = numberFormatAmount: 将数字格式化为带千分位的货币格式
 * ========================================================================================================================
 */

/**
 * 格式化浮点数
 * Tips: 可以指定保留的位数，
 * Tips: 可以配置是否舍弃末尾为 0 的小数位数
 * @param {Number} number 要格式化的数字
 * @param {Number} digit 要保留的小数位数，默认：2
 * @param {Boolean} abandonEnd 是否要舍弃末尾为 0 的小数位数，默认：true
 * @return {String} 格式化后的数字字符串
 */
export const floatFormat = (number = 0, digit = 2, abandonEnd = true) => {
    let str = Number(number).toFixed(digit);
    let parts = str.split(".");
    if (parts[1] && abandonEnd) {
        parts[1] = parts[1].replace(/0+$/, "");
    }
    if (parts[1] && parts[1].length) return parts.join(".");
    return parts[0];
};

/**
 * 将数字格式化为带千分位的货币格式
 * @param {Number} number 要格式化的数字
 * @param {String} separator 千位符
 * @return {String} 格式化后的字符串
 */
export const numberFormatAmount = (number = 0, separator = ",") => {
    let parts = number.toString().split(".");
    let integerPart = parts[0];
    let formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, separator);
    if (parts.length > 1) return `${formattedInteger}.${parts[1]}`;
    return formattedInteger;
};
