/**
 * @descript 验证工具函数
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 *
 * @list 函数列表
 * ========================================================================================================================
 * = isString               : 验证数据是否是 String 类型数据
 * = isNumber               : 验证数据是否是 Number 类型数据
 * = isArray                : 验证数据是否是 Array 类型数据
 * = isObject               : 验证数据是否是 Object 类型数据
 * = isDate                 : 验证数据是否是 Date 类型数据
 * = isFunction             : 验证数据是否是 Function 类型数据
 * = isSecondTimestamp      : 验证数据是否是秒级时间戳
 * = isMillisecondTimestamp : 验证数据是否是毫秒级时间戳
 * = isAbsoluteURL          : 校验 URL 是否是绝对 URL
 * =========================================================================================================================
 */
/**
 * 验证数据是否是 String 类型数据
 * @param {Any} variable 要验证的数据
 * @returns {Boolean} true: 是 String 类型数据；false: 不是 String 类型数据；
 */
export const isString = (variable) => typeof variable === "string";

/**
 * 验证数据是否是 Number 类型数据
 * @param {Any} variable 要验证的数据
 * @returns {Boolean} true: 是 Number 类型数据；false: 不是 Number 类型数据；
 */
export const isNumber = (variable) => !Number.isNaN(variable) && typeof variable === "number";

/**
 * 验证数据是否是 Array 类型数据
 * @param {Any} variable 要验证的数据
 * @returns {Boolean} true: 是 Array 类型数据；false: 不是 Array 类型数据；
 */
export const isArray = (variable) => Array.isArray(variable);

/**
 * 验证数据是否是 Object 类型数据
 * Tips: 不包括数组、函数、null等
 * @param {Object} variable 要验证的数据
 * @returns {Boolean} true: 是 Object 类型数据；false: 不是 Object 类型数据；
 */
export const isObject = (variable) => {
    // 排除 null 和非对象类型
    if (variable === null || typeof variable !== "object") return false;

    // 使用 Object.prototype.toString.call 来获取变量的真实类型
    // 注意这里需要排除函数类型，因为函数在JavaScript中也是对象，但通常我们不希望将它们视为普通对象
    return Object.prototype.toString.call(variable) === "[object Object]";
};

/**
 * 验证数据是否是 Date 类型数据
 * @param {Any} variable 要验证的数据
 * @returns {Boolean} true: 是 Date 类型数据；false: 不是 Date 类型数据；
 */
export const isDate = (variable) => Object.prototype.toString.call(variable) === "[object Date]";

/**
 * 验证数据是否是 Function 类型数据
 * @param {Any} variable 需要验证的数据
 * @returns {Boolean} true: 是Function 类型数据；false: 不是Function 类型数据；
 */
export const isFunction = (variable) => Object.prototype.toString.call(variable) === "[object Function]";

/**
 * 判断时间戳是否是 “秒” 级时间戳
 * Tips: 通过判断时间戳的长度（10位）来判断的
 * @param {Number | String} timestamp 时间戳
 * @return {Object} true: 是秒时间戳；false: 不是秒时间戳
 */
export const isSecondTimestamp = (timestamp) => String(timestamp).length <= 10;

/**
 * 判断时间戳是否是 “毫秒” 级时间戳
 * Tips: 通过判断时间戳的长度（13位）来判断的
 * @param {Number | String} timestamp 时间戳
 * @return {Object} true: 是毫秒时间戳；false: 不是毫秒时间戳
 */
export const isMillisecondTimestamp = (timestamp) => String(timestamp).length >= 13;

/**
 * 校验 URL 是否是绝对 URL
 * Tips: 如果 URL 以 “<scheme>：//” 或 “//”（协议相对URL）开头，则认为它是绝对的，RFC 3986 将方案名称定义为以字母开头的字符序列，然后是字母，数字，加号，句点或连字符的任意组合
 * @param {String} url 需要校验的 URL
 * @returns {Boolean} true: 是绝对 URL；false: 不是绝对 URL
 *
 */
export const isAbsoluteURL = (url = "") => /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
