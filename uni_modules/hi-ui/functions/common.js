/**
 * @descript 通用函数
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 *
 * @list 函数列表
 * ========================================================================================================================
 * = sleep : 延时函数，可以用 await 等待延时
 * ========================================================================================================================
 */
import { isFunction } from "./validate";

/**
 * 延时、等待
 * Tips: 可以用 await 等待延时，就是对 setTimeout 封装了一层 Promise
 * @param {Number} milliseconds 延时时长，单位：毫秒
 * @param {Function} fn 回调函数
 * @returns {Promise} Promise.resolve()；
 */
export const sleep = (milliseconds = 0, fn) => {
    return new Promise((resolve) => {
        let timer = setTimeout(() => {
            clearTimeout(timer);
            if (isFunction(fn)) fn();
            resolve();
        }, milliseconds);
    });
};
