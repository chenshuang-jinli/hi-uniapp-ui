/**
 * @descript 富文本内容处理工具函数集
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 *
 * @list 函数列表
 * ========================================================================================================================
 * = richtextFormatImages : 格式化富文本内容中的图片
 * ========================================================================================================================
 */

/**
 * 格式化富文本内容中的图片
 * @param {String} content 富文本内容
 * @param {String} style 图片样式，默认："width: 100%; height: auto; display: block;"
 * @returns {String} 返回格式化后的富文本内容
 */
export const richtextFormatImages = (content = "", style = "width: 100%; height: auto; display: block;") =>
    content?.replace(/<img/g, `<img style='${style}'`);
