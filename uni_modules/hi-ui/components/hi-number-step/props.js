/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 加减按钮的 hover-class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 使用 v-model 双向绑定值
    modelValue: {
        type: Number,
        default: 1
    },

    // 计数器步长
    step: {
        type: Number,
        default: 1
    },

    // 最大值
    max: {
        type: Number,
        default: Infinity
    },

    // 最小值
    min: {
        type: Number,
        default: -Infinity
    },

    // 减号按钮图标的名称
    minusIcon: {
        type: String,
        default: "__jian"
    },

    // 加号按钮图标的名称
    plusIcon: {
        type: String,
        default: "__jia"
    },

    // 是否禁用减号按钮
    disabledMinus: {
        type: Boolean,
        default: false
    },

    // 是否禁用加号按钮
    disabledPlus: {
        type: Boolean,
        default: false
    },

    // 是否显示减号按钮
    showMinus: {
        type: Boolean,
        default: true
    },

    // 是否显示加号按钮
    showPlus: {
        type: Boolean,
        default: true
    },

    // 是否禁用输入框
    disabledInput: {
        type: Boolean,
        default: false
    },

    // 是否异步变更
    async: {
        type: Boolean,
        default: false
    },

    // 宽
    width: {
        type: String,
        default: ""
    },

    // 高
    height: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 文字颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 显示边框
    border: {
        type: Boolean,
        default: true
    },

    // 边框颜色
    borderColor: {
        type: String,
        default: ""
    },

    // 聚焦时的边框颜色
    focusBorderColor: {
        type: String,
        default: ""
    },

    // 边框粗细
    borderWidth: {
        type: String,
        default: ""
    },

    // 文本对齐方式
    align: {
        type: String,
        default: ""
    },

    // 图标颜色
    iconColor: {
        type: String,
        default: ""
    },

    // 图标大小
    iconSize: {
        type: String,
        default: ""
    },

    // 图标宽度
    iconWidth: {
        type: String,
        default: ""
    },

    // 图标背景
    iconBg: {
        type: String,
        default: ""
    },

    // 加号颜色
    plusColor: {
        type: String,
        default: ""
    },

    // 加号背景
    plusBg: {
        type: String,
        default: ""
    },

    // 减号颜色
    minusColor: {
        type: String,
        default: ""
    },

    // 减号背景
    minusBg: {
        type: String,
        default: ""
    },

    // 输入框宽度
    inputWidth: {
        type: String,
        default: ""
    },

    // 输入框背景
    inputBg: {
        type: String,
        default: ""
    }
};
