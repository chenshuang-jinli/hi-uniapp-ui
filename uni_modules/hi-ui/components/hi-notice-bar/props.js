/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 文字、更多、关闭点击时的 hover-class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 显示状态
    show: {
        type: Boolean,
        default: true
    },

    // 数据
    list: {
        type: Array,
        default: () => []
    },

    // 文本字段的key
    keyName: {
        type: String,
        default: "text"
    },

    // 是否显示通知图标
    icon: {
        type: Boolean,
        default: true
    },

    // 通知图标
    iconName: {
        type: String,
        default: "__gonggao"
    },

    // 图标颜色
    iconColor: {
        type: String,
        default: ""
    },

    // 图标大小
    iconSize: {
        type: String,
        default: ""
    },

    // 是否显示更多按钮
    arrow: {
        type: Boolean,
        default: false
    },

    // 箭头图标
    arrowIcon: {
        type: String,
        default: "__you"
    },

    // 箭头颜色
    arrowColor: {
        type: String,
        default: ""
    },

    // 箭头大小
    arrowSize: {
        type: String,
        default: ""
    },

    // 是否显示关闭按钮
    close: {
        type: Boolean,
        default: false
    },

    // 关闭图标
    closeIcon: {
        type: String,
        default: "__shanchu"
    },

    // 关闭图标颜色
    closeColor: {
        type: String,
        default: ""
    },

    // 关闭图标大小
    closeSize: {
        type: String,
        default: ""
    },

    // 是否纵向
    vertical: {
        type: Boolean,
        default: false
    },

    // 滚动持续时长，单位秒，duration 和 speed 两者只能存在一个
    duration: {
        type: Number,
        default: null
    },

    // 每秒滚动距离，单位像素，duration 和 speed 两者只能存在一个
    speed: {
        type: Number,
        default: null
    },

    // 滚动间隔，单位秒
    interval: {
        type: [Number, String],
        default: 0
    },

    // 是否开启步进模式，开启步近模式后，文字默认超出一行后显示省略号
    step: {
        type: Boolean,
        default: false
    },

    // 步近模式时显示的行数
    lines: {
        type: [Number, String],
        default: 1
    }
};
