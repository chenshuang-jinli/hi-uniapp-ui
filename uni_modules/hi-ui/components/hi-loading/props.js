/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 图标
    icon: {
        type: String,
        default: "__loading"
    },

    // 文字
    text: {
        type: String,
        default: ""
    },

    // 文字颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 是否纵向
    vertical: {
        type: Boolean,
        default: false
    }
};
