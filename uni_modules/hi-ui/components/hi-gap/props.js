/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 间隔大小
    size: {
        type: String,
        default: ""
    },

    // 行内模式
    inline: {
        type: Boolean,
        default: false
    }
};
