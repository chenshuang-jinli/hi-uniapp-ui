/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 关闭按钮的 hover-class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 内容背景
    bg: {
        type: String,
        default: ""
    },

    // 内容圆角
    radius: {
        type: String,
        default: ""
    },

    // 显示状态
    show: {
        type: Boolean,
        default: false
    },

    // 模式
    // top、bottom、left、right、center
    mode: {
        type: String,
        default: "center"
    },

    // 是否显示 Header
    header: {
        type: Boolean,
        default: true
    },

    // Header 高度
    headerHeight: {
        type: String,
        default: ""
    },

    // 是否显示 Header 底部边框
    headerBorder: {
        type: Boolean,
        default: false
    },

    // Header 底部边框的颜色
    headerBorderColor: {
        type: String,
        default: ""
    },

    // Header 底部边框的粗细
    headerBorderWidth: {
        type: String,
        default: ""
    },

    // Header 底部边框的样式
    headerBorderStyle: {
        type: String,
        default: ""
    },

    // 标题
    title: {
        type: String,
        default: ""
    },

    // 标题颜色
    titleColor: {
        type: String,
        default: ""
    },

    // 标题大小
    titleFontSize: {
        type: String,
        default: ""
    },

    // 标题粗细
    titleFontWeight: {
        type: [String, Number],
        default: ""
    },

    // 是否显示关闭按钮
    close: {
        type: Boolean,
        default: true
    },

    // 关闭按钮图标名称
    closeIcon: {
        type: String,
        default: "__shanchu"
    },

    // 关闭按钮图标颜色
    closeColor: {
        type: String,
        default: ""
    },

    // 关闭按钮图标大小
    closeSize: {
        type: String,
        default: ""
    },

    // 关闭按钮图标位置
    closeRight: {
        type: String,
        default: ""
    },

    // 关闭按钮图标位置
    closeTop: {
        type: String,
        default: ""
    },

    // 关闭按钮图标位置
    closeBottom: {
        type: String,
        default: ""
    },

    // 关闭按钮图标位置
    closeLeft: {
        type: String,
        default: ""
    },

    // 是否显示 Footer
    footer: {
        type: Boolean,
        default: true
    },

    // Footer 高度
    footerHeight: {
        type: String,
        default: ""
    },

    // 是否显示 Footer 顶部边框
    footerBorder: {
        type: Boolean,
        default: false
    },

    // Footer 顶部边框的颜色
    footerBorderColor: {
        type: String,
        default: ""
    },

    // Footer 顶部边框的粗细
    footerBorderWidth: {
        type: String,
        default: ""
    },

    // Footer 顶部边框的样式
    footerBorderStyle: {
        type: String,
        default: ""
    },

    // 高度
    height: {
        type: String,
        default: ""
    },

    // 最大高度
    maxHeight: {
        type: String,
        default: ""
    },

    // 宽度
    width: {
        type: String,
        default: ""
    },

    // 最大宽度
    maxWidth: {
        type: String,
        default: ""
    },

    // 是否显示遮罩
    mask: {
        type: Boolean,
        default: true
    },

    // 遮罩是否可点击关闭
    maskClickable: {
        type: Boolean,
        default: true
    },

    // 遮罩的背景
    maskBg: {
        type: String,
        default: ""
    }
};
