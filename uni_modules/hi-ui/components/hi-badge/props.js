/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // hover-class
    hover: {
        type: String,
        default: ""
    },

    // 值
    value: {
        type: [Number, String],
        default: 0
    },

    // 值为 0 时是否显示
    zero: {
        type: Boolean,
        default: false
    },

    // 模式。
    // dot: 圆点模式
    // overflow: 值大于 max 时显示 ${max}+
    // ellipsis: 值大于 max 时显示 ${max}...
    // limit: 以 limit 属性的值作为判断条件，值大于 limit 时显示 ${value / limit} + ${suffix}
    mode: {
        type: String,
        default: ""
    },

    // 最大值
    max: {
        type: [Number, String],
        default: 99
    },

    // limit 分割值
    limit: {
        type: [Number, String],
        default: 1000
    },

    // limit 分割后的后缀
    suffix: {
        type: String,
        default: "k"
    },

    // limit 后要保留的小数位数
    decimals: {
        type: Number,
        default: 2
    },

    // limit 后是否舍弃末尾为0的小数位数
    discardLastZero: {
        type: Boolean,
        default: true
    },

    // 是否开启 absolute 模式
    absolute: {
        type: Boolean,
        default: false
    },

    // top
    top: {
        type: String,
        default: ""
    },

    // right
    right: {
        type: String,
        default: ""
    },

    // bottom
    bottom: {
        type: String,
        default: ""
    },

    // left
    left: {
        type: String,
        default: ""
    },

    // transform
    transform: {
        type: String,
        default: ""
    },

    // 主题
    theme: {
        type: String,
        default: ""
    },

    // 镂空？
    plain: {
        type: Boolean,
        default: false
    },

    // 浅化
    tint: {
        type: Boolean,
        default: false
    },

    // 浅化透明度
    tintOpacity: {
        type: [String, Number],
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 圆角值
    radius: {
        type: String,
        default: ""
    },

    // 高度
    height: {
        type: String,
        default: ""
    },

    // 宽度
    width: {
        type: String,
        default: ""
    },

    // 圆点模式时的尺寸
    dotSize: {
        type: String,
        default: ""
    },

    // 是否显示边框
    border: {
        type: Boolean,
        default: false
    },

    // 边框颜色
    borderColor: {
        type: String,
        default: ""
    },

    // 边框宽度
    borderWidth: {
        type: String,
        default: ""
    },

    // 边框类型
    borderStyle: {
        type: String,
        default: ""
    }
};
