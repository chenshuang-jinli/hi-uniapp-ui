/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 数据列表长度
    total: {
        type: Number,
        default: 0
    },

    // 列数
    cols: {
        type: [Number, String],
        default: ""
    },

    // 行/列间距
    gap: {
        type: String,
        default: ""
    },

    // 是否显示空内容
    showEmpty: {
        type: Boolean,
        default: true
    },

    // 空内容配置项
    emptyOpts: {
        type: Object,
        default: () => ({})
    },

    // 是否显示加载状态
    showStatus: {
        type: Boolean,
        default: true
    },

    // 数据加载状态
    status: {
        type: String,
        default: ""
    },

    // 加载状态组件的配置
    statusOpts: {
        type: Object,
        default: () => ({})
    }
};
