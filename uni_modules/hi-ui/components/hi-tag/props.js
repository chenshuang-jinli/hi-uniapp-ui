/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 标签和关闭按钮的 hover-class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 标签文本
    text: {
        type: String,
        default: ""
    },

    // 图标
    icon: {
        type: String,
        default: ""
    },

    // 是否可关闭
    // 组件只提供关闭事件，关闭逻辑需自行处理
    close: {
        type: Boolean,
        default: false
    },

    // 关闭图标的名称
    closeIcon: {
        type: String,
        default: "__shanchu"
    },

    // 关闭按钮 absolute
    closeAbsolute: {
        type: Boolean,
        default: false
    },

    // 主题
    theme: {
        type: String,
        default: ""
    },

    // 镂空？
    plain: {
        type: Boolean,
        default: false
    },

    // 浅化
    tint: {
        type: Boolean,
        default: false
    },

    // 浅化透明度
    tintOpacity: {
        type: [String, Number],
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 圆角值
    radius: {
        type: String,
        default: ""
    },

    // 高度
    height: {
        type: String,
        default: ""
    },

    // 宽度
    width: {
        type: String,
        default: ""
    },

    // 是否显示边框
    border: {
        type: Boolean,
        default: false
    },

    // 边框颜色
    borderColor: {
        type: String,
        default: ""
    },

    // 边框宽度
    borderWidth: {
        type: String,
        default: ""
    },

    // 边框类型
    borderStyle: {
        type: String,
        default: ""
    }
};
