/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 数量
    count: {
        type: Number,
        default: 0
    },

    // 激活项下标
    current: {
        type: Number,
        default: 0
    },

    // 指示器类型
    // dots: 点指示器
    // line: 线指示器
    mode: {
        type: String,
        default: "dots"
    },

    // 位置
    // bottom-center: 底部; bottom-left: 底部靠左; bottom-right: 底部靠右; left-center: 左侧居中; left-bottom: 左侧靠下; right-center: 右侧居中; right-bottom: 右侧靠下;
    position: {
        type: String,
        default: "bottom-center"
    },

    // 是否开启 absolute
    absolute: {
        type: Boolean,
        default: false
    },

    // 位置
    top: {
        type: String,
        default: ""
    },

    // 位置
    right: {
        type: String,
        default: ""
    },

    // 位置
    bottom: {
        type: String,
        default: ""
    },

    // 位置
    left: {
        type: String,
        default: ""
    },

    // 未激活时的颜色
    inactiveColor: {
        type: String,
        default: ""
    },

    // 激活时的颜色
    activeColor: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 粗细
    weight: {
        type: String,
        default: ""
    },

    // 间距
    gap: {
        type: String,
        default: ""
    },

    // 未激活时的大小
    // 点指示器时根据布局位置表示宽度或高度
    inactiveSize: {
        type: String,
        default: ""
    },

    // 激活时的大小
    // 点指示器时根据布局位置表示宽度或高度
    activeSize: {
        type: String,
        default: ""
    },

    // 根据布局位置表示线指示器的整体宽度或高度
    lineSize: {
        type: String,
        default: ""
    },

    // 根据布局位置表示线指示器 item 的宽度或高度
    lineItemSize: {
        type: String,
        default: ""
    }
};
