/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 主题
    theme: {
        type: String,
        default: "default"
    },

    // 单位和价格之间的间距
    gap: {
        type: String,
        default: ""
    },

    // 颜色
    color: {
        type: String,
        default: ""
    },

    // 大小
    fontSize: {
        type: String,
        default: ""
    },

    // 粗细
    fontWeight: {
        type: String,
        default: ""
    },

    // 价格单位
    unit: {
        type: String,
        default: "¥"
    },

    // 价格
    value: {
        type: [String, Number],
        default: 0
    },

    // 单位大小
    unitSize: {
        type: [String, Number],
        default: ""
    },

    // 单位颜色
    unitColor: {
        type: String,
        default: ""
    },

    // 单位粗细
    unitWeight: {
        type: String,
        default: ""
    },

    // 小数点位数的长度
    decimals: {
        type: [String, Number],
        default: 2
    },

    // 千位符
    thousands: {
        type: String,
        default: ","
    },

    // 是否显示固定的小数位数，关闭后，小数位为0的则不显示
    fixed: {
        type: Boolean,
        default: false
    },

    // 是否显示删除线？
    line: {
        type: Boolean,
        default: false
    },

    // 线的颜色
    lineColor: {
        type: String,
        default: ""
    },

    // 线的粗细
    lineHeight: {
        type: String,
        default: ""
    },

    // 对齐方式
    align: {
        type: String,
        default: ""
    }
};
