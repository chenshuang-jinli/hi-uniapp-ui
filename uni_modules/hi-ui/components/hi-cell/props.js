/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // hover class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 内边距
    padding: {
        type: String,
        default: ""
    },

    // 文字颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 左侧图标
    leftIcon: {
        type: String,
        default: ""
    },

    // 左侧图标颜色
    leftIconColor: {
        type: String,
        default: ""
    },

    // 左侧图标大小
    leftIconSize: {
        type: String,
        default: ""
    },

    // 标题
    title: {
        type: String,
        default: ""
    },

    // 标题颜色
    titleColor: {
        type: String,
        default: ""
    },

    // 标题大小
    titleFontSize: {
        type: String,
        default: ""
    },

    // 标题加粗
    bold: {
        type: Boolean,
        default: false
    },

    // 描述
    desc: {
        type: String,
        default: ""
    },

    // 描述颜色
    descColor: {
        type: String,
        default: ""
    },

    // 描述文字大小
    descFontSize: {
        type: String,
        default: ""
    },

    // 提示
    tips: {
        type: String,
        default: ""
    },

    // 提示颜色
    tipsColor: {
        type: String,
        default: ""
    },

    // 提示大小
    tipsFontSize: {
        type: String,
        default: ""
    },

    // 右侧图标
    rightIcon: {
        type: String,
        default: ""
    },

    // 右侧图标颜色
    rightIconColor: {
        type: String,
        default: ""
    },

    // 右侧图标大小
    rightIconSize: {
        type: String,
        default: ""
    },

    // 右侧图标旋转角度
    rotate: {
        type: [Number, String],
        default: ""
    }
};
