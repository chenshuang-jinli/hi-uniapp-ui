/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // hover-class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 列表
    list: {
        type: Array,
        default: () => []
    },

    // 下标
    current: {
        type: Number,
        default: 0
    },

    // 显示字段的key
    keyName: {
        type: String,
        default: "name"
    },

    // 右侧选项卡数据
    right: {
        type: Array,
        default: () => []
    },

    // 是否开启自动滚动
    autoScroll: {
        type: Boolean,
        default: true
    },

    // 是否纵向布局
    column: {
        type: Boolean,
        default: false
    },

    // 等分布局，只适用于横向滚动模式
    equal: {
        type: Boolean,
        default: false
    },

    // 两端对齐，只适用于横向滚动模式
    justify: {
        type: Boolean,
        default: false
    },

    // 开启滚动动画
    scrollWithAnimation: {
        type: Boolean,
        default: true
    },

    // 宽
    width: {
        type: String,
        default: ""
    },

    // 高
    height: {
        type: String,
        default: ""
    },

    // 选项之间的间距
    gap: {
        type: String,
        default: "15px"
    },

    // 选项内边距
    itemPadding: {
        type: String,
        default: ""
    },

    // 选项背景
    itemBg: {
        type: String,
        default: ""
    },

    // 激活项背景
    activeItemBg: {
        type: String,
        default: ""
    },

    // 文字颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 文字粗细
    fontWeight: {
        type: String,
        default: ""
    },

    // 激活的文字颜色
    activeColor: {
        type: String,
        default: ""
    },

    // 激活的文字大小
    activeFontSize: {
        type: String,
        default: ""
    },

    // 激活的文字粗细
    activeFontWeight: {
        type: String,
        default: ""
    },

    // 指示器的宽度
    barWidth: {
        type: String,
        default: ""
    },

    // 指示器的高度
    barHeight: {
        type: String,
        default: ""
    },

    // 指示器的颜色
    barColor: {
        type: String,
        default: ""
    },

    // 指示器圆角值
    barRadius: {
        type: String,
        default: ""
    },

    // 指示器的位置
    barLeft: {
        type: String,
        default: ""
    },

    barBottom: {
        type: String,
        default: ""
    },

    itemStyle: {
        type: [String, Object, Array],
        default: ""
    },

    activeItemStyle: {
        type: [String, Object, Array],
        default: ""
    },

    // 图标颜色
    iconColor: {
        type: String,
        default: ""
    },

    // 图标大小
    iconSize: {
        type: String,
        default: ""
    },

    // 激活的图标颜色
    activeIconColor: {
        type: String,
        default: ""
    },

    // 激活的图标大小
    activeIconSize: {
        type: String,
        default: ""
    }
};
